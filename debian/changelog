node-qs (6.13.0+ds+~6.9.16-1+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 08 Mar 2025 21:32:32 +0000

node-qs (6.13.0+ds+~6.9.16-1) unstable; urgency=medium

  * Team upload
  * Declare compliance with policy 4.7.0
  * New upstream version 6.13.0+ds+~6.9.16
  * Update test

 -- Yadd <yadd@debian.org>  Sat, 14 Sep 2024 16:48:33 +0400

node-qs (6.11.0+ds+~6.9.7-4) unstable; urgency=medium

  * Update standards version to 4.6.2, no changes needed.
  * Fix build links (Closes: #1057584)

 -- Yadd <yadd@debian.org>  Thu, 07 Dec 2023 07:18:47 +0400

node-qs (6.11.0+ds+~6.9.7-3+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Apr 2023 22:15:43 +0000

node-qs (6.11.0+ds+~6.9.7-3) unstable; urgency=medium

  * Team upload
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update build for node-deep-equal 2.1

 -- Yadd <yadd@debian.org>  Sun, 04 Dec 2022 18:35:21 +0100

node-qs (6.11.0+ds+~6.9.7-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 22 Nov 2022 01:32:31 +0000

node-qs (6.11.0+ds+~6.9.7-1) unstable; urgency=medium

  * Team upload
  * Update build links
  * New upstream version 6.11.0+ds+~6.9.7

 -- Yadd <yadd@debian.org>  Sat, 09 Jul 2022 09:49:44 +0200

node-qs (6.10.3+ds+~6.9.7-1) unstable; urgency=medium

  * Team upload
  * New upstream version 6.10.3+ds+~6.9.7
  * Drop embedded test modules, now available

 -- Yadd <yadd@debian.org>  Thu, 13 Jan 2022 07:41:26 +0100

node-qs (6.10.2+ds+~6.9.7-1) unstable; urgency=medium

  * Team upload

  [ Ayoyimika Ajibade ]
  * Updated build dependency

  [ Yadd ]
  * Declare compliance with policy 4.6.0
  * Fix filenamemangle
  * Embed typescript definitions and repack
  * New upstream version 6.10.2+ds+~6.9.7
  * Fix build links
  * Provides libjs-qs

 -- Yadd <yadd@debian.org>  Wed, 08 Dec 2021 15:08:58 +0100

node-qs (6.10.1+ds-1) unstable; urgency=medium

  * Team upload

  [ Yadd ]
  * Declare compliance with policy 4.5.1
  * Fix GitHub tags regex

  [ Ayoyimika Ajibade ]
  * New upstream version 6.10.1+ds
  * Added package to extcopies file.
  * Updated test command
  * Added node_modules/ for clean build
  * Updated standards version 4.5.1 => 4.6.0.1

 -- Ayoyimika Ajibade <ayoyimikaajibade@gmail.com>  Thu, 09 Sep 2021 11:20:51 +0000

node-qs (6.9.4+ds-1+deb11u1+apertis0) apertis; urgency=medium

  * Sync from debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Wed, 25 Jan 2023 12:57:15 +0000

node-qs (6.9.4+ds-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 18:04:59 +0000

node-qs (6.9.4+ds-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit.
  * Update standards version to 4.5.0, no changes needed.

  [ Xavier Guimard ]
  * Bump debhelper compatibility level to 13
  * Modernize debian/watch
  * Use dh-sequence-nodejs
  * New upstream version 6.9.4+ds
  * Apply multi-arch hints (foreign)

 -- Xavier Guimard <yadd@debian.org>  Mon, 16 Nov 2020 17:41:51 +0100

node-qs (6.9.1+ds-1) unstable; urgency=medium

  * Team upload
  * New upstream version 6.9.1+ds

 -- Xavier Guimard <yadd@debian.org>  Thu, 05 Dec 2019 07:09:52 +0100

node-qs (6.9.0+ds-1) unstable; urgency=medium

  * Team upload
  * Bump debhelper compatibility level to 12
  * Declare compliance with policy 4.4.1
  * Add debian/gbp.conf
  * Add upstream/metadata
  * Fix debian/copyright
  * Switch install to pkg-js-tools
  * Generate dist/qs.js using browserify-lite
  * New upstream version 6.9.0+ds
  * Enable upstream test

 -- Xavier Guimard <yadd@debian.org>  Sun, 20 Oct 2019 16:00:40 +0200

node-qs (6.5.2-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sun, 21 Feb 2021 11:09:19 +0000

node-qs (6.5.2-1) unstable; urgency=medium

  * Team upload
  * New upstream version 6.5.2
  * Use salsa.debian.org in Vcs-* fields
  * Bump debhelper compatibility level to 11
  * Bump Standards-Version to 4.2.1 (no changes needed)
  * Change section to javascript and priority to optional

 -- Pirate Praveen <praveen@debian.org>  Wed, 19 Sep 2018 18:20:24 +0530

node-qs (2.2.4-1) unstable; urgency=medium

  * Imported Upstream version 2.2.4
  * Move to pkg-javascript
  * Standards-Version 3.9.6
  * New upstream, project rewritten from scratch:
    + Homepage updated
    + watch file updated
    + copyright changed to BSD-3-clause
  * Rewrite description
  * Build-Depends nodejs
  * Update docs
  * Remove examples
  * Update install files
  * Disable test and build auto targets
  * Fix permissions of installed files
  * Add autopkgtest

 -- Jérémy Lal <kapouer@melix.org>  Sun, 12 Oct 2014 16:59:09 +0200

node-qs (0.6.5-1) unstable; urgency=low

  * Upstream update
  * watch: use github directly
  * control:
    + Homepage to github
    + Bump Standards-Version to 3.9.4, no changes required
    + Canonicalize Vcs-* url
    + Use Node.js in description
  * copyright: add Upstream-Contact, Source fields.

 -- Jérémy Lal <kapouer@melix.org>  Wed, 14 Aug 2013 11:31:02 +0200

node-qs (0.5.5-1) experimental; urgency=low

  * Upstream update
  * Copyright: rename MIT to Expat license
  * Install examples.js
  * Install the module file in qs/index.js, remove install override.

 -- Jérémy Lal <kapouer@melix.org>  Sat, 23 Mar 2013 21:11:26 +0100

node-qs (0.4.2-1) unstable; urgency=low

  * New upstream version
  * Standards-Version bump to 3.9.3, no changes needed
  * Update debian/copyright

 -- David Paleino <dapal@debian.org>  Thu, 22 Mar 2012 22:42:39 +0100

node-qs (0.4.0-1) unstable; urgency=low

  * New upstream version

 -- David Paleino <dapal@debian.org>  Sat, 31 Dec 2011 11:41:53 +0100

node-qs (0.3.2-1) unstable; urgency=low

  * New upstream version
  * Add debian/watch

 -- David Paleino <dapal@debian.org>  Wed, 16 Nov 2011 22:41:43 +0100

node-qs (0.3.1-2) unstable; urgency=low

  * Compliance to Debian Javascript Policy

 -- David Paleino <dapal@debian.org>  Sun, 16 Oct 2011 00:00:18 +0200

node-qs (0.3.1-1) unstable; urgency=low

  * Initial release (Closes: #645178)

 -- David Paleino <dapal@debian.org>  Thu, 13 Oct 2011 12:13:09 +0200
